# Lab2 DevOps, ITMO, 8th-Semester

### First build the image:

```
docker build -t mynginxtest:latest .
```

### Run the command to bring up the containers:

```
docker-compose up -d
```

### To generate a certificate run:

```
docker-compose run --rm certbot certonly --webroot --webroot-path /var/www/certbot/ -d example.org
```

### To renew a certificate run:

```
docker-compose run --rm certbot renew
```

### To bring down everything run:

```
docker-compose down
```