FROM scratch
ADD ubuntu-base-22.04-base-amd64.tar.gz /

RUN apt-get update \
    && apt-get install --no-install-recommends -y nginx \
    && rm -rf /var/lib/apt/lists/*

CMD ["nginx", "-g", "daemon off;"]